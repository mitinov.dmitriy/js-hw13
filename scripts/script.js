window.addEventListener("load", function () {

    const pageContainer = document.querySelector(".container");
    const colorButton = document.querySelector(".change_theme_button");
    const navLinks = document.querySelectorAll(".header_menu_link");

    if (localStorage.length) {
        pageContainer.classList.add(localStorage.getItem("classAdded"));
        navLinks.forEach((el) => {
            el.classList.add(localStorage.getItem("classAddedNav"));
        })
    }

    colorButton.addEventListener("click", changeColorTheme);

    function changeColorTheme() {
        if (!localStorage.length) {
            pageContainer.classList.add("dark_colors");
            navLinks.forEach((el) => {
                el.classList.add("header_menu_link_dark_mode");
            })
            localStorage.setItem("classAdded", "dark_colors");
            localStorage.setItem("classAddedNav", "header_menu_link_dark_mode");
        } else {
            pageContainer.classList.remove("dark_colors");
            navLinks.forEach((el) => {
                el.classList.remove("header_menu_link_dark_mode");
            })
            localStorage.removeItem("classAdded");
            localStorage.removeItem("classAddedNav");
        }
    }
});
